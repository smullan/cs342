//
//  GameScene.swift
//  gameTest
//
//
// Sean Mullan & Alex Mathson

import SpriteKit

let circleCategory: UInt32 = 0x1
let edgeCategory: UInt32 = 0x1<<1

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var circle : SKSpriteNode?
    var centerLoc : CGPoint = CGPointMake(0, 0)
    var push : CGVector = CGVectorMake(0, 0)
    var sent : Int = 0

    func didBeginContact(contact: SKPhysicsContact) {
        //
    }
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        self.backgroundColor = SKColor(red: 0.67, green: 0.92, blue: 0.55, alpha: 1)
        
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect:view.frame);
        self.physicsWorld.gravity = CGVectorMake(0, -5);
        self.physicsWorld.contactDelegate = self
        self.makeCircle(view.frame.size)
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        circle!.physicsBody!.velocity = CGVectorMake(0, 0)
        var touch = touches.first as! UITouch
        var location = touch.locationInView(self.view)
        centerLoc = location

    }
   
    override func update(currentTime: CFTimeInterval) {
    }
    
    func makeCircle(size :CGSize){
        circle = SKSpriteNode(imageNamed: "Ball")
        circle!.position = CGPointMake(size.width/2, size.height/2);
        circle!.physicsBody = SKPhysicsBody(rectangleOfSize: circle!.size)
        circle!.physicsBody!.linearDamping = 0.6
        circle!.physicsBody!.friction = 0.2
        circle!.physicsBody!.categoryBitMask = circleCategory;
        circle!.physicsBody!.collisionBitMask = 0xFFFFFFFF-circleCategory
        self.addChild(circle!)
        
        
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        var touch = touches.first as! UITouch
        var location = touch.locationInView(self.view)
        var prevLocation = centerLoc
        var dist = CGPointMake(location.x-prevLocation.x, -(location.y-prevLocation.y))
        var threshold : CGFloat = 100.00
        println("dist x \(dist.x)  y \(dist.y)")
        if (dist.x>threshold){
            dist.x=threshold;
        }
        else if (dist.x<(-1*threshold)){
            dist.x = -threshold;
        }
        if(dist.y>threshold){
            dist.y=threshold;
        }else if(dist.y<(-1*threshold)){
            dist.y = -threshold;
        }
        dist.x = dist.x/20
        dist.y = dist.y/20
        push = CGVectorMake(dist.x, dist.y);
        circle?.physicsBody?.applyImpulse(push);

        
    }


}


