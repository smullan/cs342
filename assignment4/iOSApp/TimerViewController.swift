//
//  TimerViewController.swift
//  iOSApp
//
// Sean Mullan & Alex Mathson


import Foundation
import UIKit

class TimerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate{
    
//    var timer = NSTimer(timeInterval: 1.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
    var timer : NSTimer = NSTimer()
    @IBOutlet weak var timerPicker: UIPickerView!
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timerPicker.delegate = self
        timerPicker.dataSource = self
        timerPicker.layer.borderColor = UIColor.grayColor().CGColor
        timerPicker.layer.borderWidth = 5.0
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func startButtonPressed(sender: AnyObject) {
        if(startButton.currentTitle == "Start"){
            startButton.setTitle("Stop", forState: .Normal)
            timer = NSTimer(timeInterval: 1, target: self, selector: "updateTimer", userInfo: nil, repeats: true)
            NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
            timer.fire()
        }else{
            startButton.setTitle("Start", forState: .Normal)
            timer.invalidate()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 60
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var columnView : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width/4, 0, 2*self.view.frame.size.width/4, 20));
        columnView.text = NSString(format: "%lu", row) as String
        columnView.textAlignment = .Center
        return columnView
    }
    func updateTimer(){
        var seconds = timerPicker.selectedRowInComponent(1)
        var minutes = timerPicker.selectedRowInComponent(0)
        if (seconds == 0){
            if (minutes == 0){
                startButton.setTitle("Start", forState: .Normal)
                timer.invalidate()
            }else{
                timerPicker.selectRow(59, inComponent: 1, animated: true)
                timerPicker.selectRow(minutes-1, inComponent: 0, animated: true)
            }
        }else{
            timerPicker.selectRow(seconds-1, inComponent: 1, animated: true)
        }
    }
}
