//
//  MapViewController.swift
//  iOSApp
//
// Sean Mullan & Alex Mathson


import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Do more stuff here
        var center = CLLocationCoordinate2D(latitude: 44.46199,longitude: -93.154619)
        var span = MKCoordinateSpan(
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
        )
        var northfield = MKCoordinateRegion(center: center, span: span)
        mapView.setRegion(northfield, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
