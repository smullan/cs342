package com.example.mullans.myapplication;

/**
 * Created by dangquang2011 on 07.04.15.
 */
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MyFragment fragment = new MyFragment();
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fragment).commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    public class MyFragment extends ListFragment {
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            ArrayList<String> listItems = new ArrayList<String>();
            listItems.add("Number Activity");
            listItems.add("Star Activity");
            listItems.add("Sound Activity");


            ArrayAdapter adapter = new MyAdapter(getActivity().getApplicationContext(),R.layout.list_cell, listItems);
            ListView listView = (ListView)getView().findViewById(android.R.id.list);
            this.setListShown(true);
            listView.setAdapter(adapter);
        }

        public void onListItemClick(ListView listView, View view, int position, long id) {

            if (id == 0) {
                Intent intent = new Intent(getActivity().getApplicationContext(), NumberActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }else if(id == 1){
                Intent intent = new Intent(getActivity().getApplicationContext(), StarActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }else if(id == 2){
                Intent intent = new Intent(getActivity().getApplicationContext(), SoundActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }
        }
    }
}
