package com.example.mullans.myapplication;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class StarActivity extends ActionBarActivity implements SeekBar.OnSeekBarChangeListener{

    int red;
    int green;
    int blue;
    int alpha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star);
        red = 0;
        green = 0;
        blue = 0;
        alpha = 255;
        ImageView myView = (ImageView)findViewById(R.id.imageView);
        myView.setColorFilter(Color.argb(alpha, red, green, blue));
        final SeekBar sk1=(SeekBar) findViewById(R.id.seekBar);
        final SeekBar sk2=(SeekBar) findViewById(R.id.seekBar2);
        final SeekBar sk3=(SeekBar) findViewById(R.id.seekBar3);

        sk1.setOnSeekBarChangeListener(this);
        sk2.setOnSeekBarChangeListener(this);
        sk3.setOnSeekBarChangeListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        noinspection SimplifiableIfStatement
        this.finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        ImageView myView = (ImageView)findViewById(R.id.imageView);
        int progress = seekBar.getProgress();
//                System.out.println("here3");
        progress = (int)(progress/100.0 * 255);
        if(seekBar.getId()==R.id.seekBar){
            green = progress;
            TextView greenView = (TextView)findViewById(R.id.greenText);
            greenView.setText("Green: "+String.valueOf(green));

        }else if(seekBar.getId()==R.id.seekBar2){
            blue = progress;
            TextView blueView = (TextView)findViewById(R.id.blueText);
            blueView.setText("Blue: "+String.valueOf(blue));

        }else if(seekBar.getId()==R.id.seekBar3){
            red = progress;
            TextView redView = (TextView)findViewById(R.id.redText);
            redView.setText("Red: "+String.valueOf(red));

        }
        myView.setColorFilter(Color.argb(alpha, red, green, blue));


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
