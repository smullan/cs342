package com.example.mullans.myapplication;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;


import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Random;


public class NumberActivity extends ActionBarActivity implements View.OnClickListener {

    private int answer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number);
        Button button = (Button)this.findViewById(R.id.go_button);
        button.setOnClickListener(this);
        Random rand = new Random();
        answer = rand.nextInt(101);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        this.finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // Drop the keyboard and get N
        int guessedInteger;
        this.hideKeyboard();
        EditText integerTextField = (EditText)this.findViewById(R.id.integer_edit_text);
        String integerString = integerTextField.getText().toString();
        guessedInteger = Integer.parseInt(integerString);
        String resultMessage;
        if (guessedInteger == answer){
            resultMessage = String.format("You got it right!");
        }
        else if (guessedInteger < answer){
            resultMessage = String.format("Too low!");
        }
        else{
            resultMessage = String.format("Too high!");
        }
        TextView resultTextView = (TextView)findViewById(R.id.result_text_view);
        resultTextView.setText(resultMessage);
    }

    public void hideKeyboard() {
        EditText myEditText = (EditText)this.findViewById(R.id.integer_edit_text);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

}