package com.example.mullans.myapplication;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.FileInputStream;
import java.io.IOException;


public class SoundActivity extends ActionBarActivity implements ImageButton.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageButton button1 = (ImageButton)findViewById(R.id.imageButton);
        button1.setOnClickListener(this);
        ImageButton button2 = (ImageButton)findViewById(R.id.imageButton2);
        button2.setOnClickListener(this);
        ImageButton button3 = (ImageButton)findViewById(R.id.imageButton3);
        button3.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
        this.finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view==(ImageButton)findViewById(R.id.imageButton)){
            MediaPlayer mplayer = MediaPlayer.create(this,R.raw.meow);
            mplayer.start();
        }else if (view==(ImageButton)findViewById(R.id.imageButton2)){
            MediaPlayer mplayer = MediaPlayer.create(this,R.raw.elephant);
            mplayer.start();

        }else if (view==(ImageButton)findViewById(R.id.imageButton3)){
            MediaPlayer mplayer = MediaPlayer.create(this,R.raw.moo);
            mplayer.start();
        }
    }
}
