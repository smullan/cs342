package com.example.mullans.myapplication;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Mullan on 4/9/15.
 */
public class MyAdapter extends ArrayAdapter<String> {
    public MyAdapter(Context context, int resource, ArrayList<String> listItems) {
        super(context, resource, listItems);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_cell, null);
        }

        if (v != null) {
            TextView textView = (TextView) v.findViewById(R.id.listCellTextView);
            textView.setText(this.getItem(position));
            textView.setTextColor(Color.BLACK);
        }

        return v;
    }
}
